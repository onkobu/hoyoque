Host your own questionnaire

1. Sketch your questionnaire as XML
1. Run the processor
1. Push HTML and PHP to your web space
1. Get your SQLite database filled
1. Aggregate results

All questionnaires are active in a certain time range. Before that it is not possible to answer any questions. After the time elapsed it shows only statistics of answers.

To take part each questionnaire provides a limited number of tokens. Hand this over to your participants, one token per person. A filled in survey-form will only be saved with a valid token. Each token can be used only once. It is quite hard to fill in the form wrongly and waste tokens. Upon connection issues the token cannot be used, thus it stays valid.

As soon as a token was used, it is deleted from the database. It is not possible to recover it and the answers are not linked to a token. There is not even a log file or recording mechanism for IPs, cookies or similar. It works even without JavaScript. All validation happens in the backend.

A malicious attacker must use tokens, too. Without a token no writing action takes place. There is no separate notification, whether values were valid or not. Only the valid token is displayed as consumed. Tokens are created from a random source.

# Invocation

Necessary components:

- Bash-environment (Cygwin works, too)
- saxonb-xslt processor command available, to transform XML to HTML/ PHP/ DDL
- sqlite3 command available, to fill the database
- /dev/urandom-device, to create tokens

Currently it is not possible to run this generator on Windows right away.

# Syntax

## Structure

- outer tag hoyoque
- header and footer
- blocks: configuration, enumeration of section
- enumeration of question inside section

```
<hoyoque><section title="About You">
<!-- A simple question -->
<question name="gender" type="gender">
	<label>
		What is your gender?
	</label>
	<info>
		You can skip this question.
	</info>
</question>
</section></hoyoque>
```

## Configuration

With the configuration you have to limit the survey's visibility, stylesheet and a few database parameters.

- survey in general , start- and end-time
- database, number of tokens
- HTML, e.g. CSS

## Header and Footer

The header provides a description of the survey, maybe its purpose. The footer contains legal information.

## Question Types

All question types can be skipped or support an empty selection.

- oneofmany – a list of options to choose from
- yesno – yes or no
- rangeXtoY – various ranges, e.g. -2 to 2, 0 to 10, 1 to 6 or 6 to 1
- gender – classic male/ female pair

## Small Example

A simple survey with two sections and a question per section. Have look at the folder examples for longer surveys, types and structures.

```
<hoyoque>
	<configuration identifier="one_page"
				webroot="www/onkobu/hoyoque"
				validFrom="2020-04-14T17:00:00+00:00"
				validTo="2020-04-14T17:35:00+00:00">
		<database relativeDir="../../../"
			tokens="16"/>
		<html stylesheet="questionnaire.css"/>
		<php />
	</configuration>

	<head>
		<title>One-Page-Example</title>
		<description>
		It only takes 5 minutes or less to answer the
				questions below. You can submit it only once.
		</description>
	</head>

	<section title="About You">
		<question name="firsttime" type="yesno">
			Is this your first time filling in a questionnaire online?
		</question>

	</section>
	<section title="About the Survey">
		<question name="usability" type="range2to2">
			<label>
					How usable is this survey?
			</label>
			<info>
					Choose a negative value if it is not usable. Use a positive value if very usable.
			</info>
		</question>

	</section>

	<foot>
		This questionnaire works without Cookies, does
		not save your IP address and is fully anonymous.
	</foot>
</hoyoque>
```
