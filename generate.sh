#!/bin/bash

function output_tokens {
	tokenCount=$1
	echo "DELETE FROM hoyoque_token;"
	echo ""
for (( tkn=0; tkn<tokenCount; tkn++)); do
	
	while : ; do 
		token=$(head -n1 /dev/urandom | tr -cd '[:alnum:]' | cut -c -10)
		[[ ${#token} -lt 10 ]] || break
	done

	echo "INSERT INTO hoyoque_token (token) VALUES ( '$token' );"
done
}

function check_errors {
	errCode=$1
	if [ "$errCode" -ne 0 ]; then
		exit "$errCode"
	fi
}

function print_usage_and_exit {
	echo ""
	echo "$(basename $0) -i <input-xml> -o <output-dir>"
	echo ""
	echo "Transform a XML-based questionnaire into a PHP-based web form."
	echo "All submitted forms will be put into a SQLite backend."
	echo ""
	echo "input-xml  - XML formatted questionnaire description"
	echo "output-dir - Output directory, creates structure recursively inside"
	echo ""
	exit 0
}

msg=""

hash saxonb-xslt 2>/dev/null || msg='saxonb-xslt not found' 
hash sqlite3 2>/dev/null ||  msg="$msg\nsqlite3 not found" 

if [ ! -z "$msg" ]; then
	echo -e "$msg"
	echo "Provide command(s) and retry, exiting now"
	exit 3
fi

while getopts "i:o:" opt; do
	case $opt in
    	i ) inputFile=$OPTARG;;	
		o ) outputDir=$OPTARG;;
		* ) echo "Unimplemented option chosen."; exit 7;;   # Default.
	esac
done

if [ -z "$inputFile" ] || [ -z "$outputDir" ]; then
	print_usage_and_exit
fi

webroot=
identifier=
language="en"
eval "$(saxonb-xslt -xsl:generator/transform_shellconf.xsl $inputFile)"

confErrs=
if [ ! -f styles/$stylesheet ]; then
	confErrs="Stylesheet $stylesheet not found under directory stylesheets\n"
fi

if [ -z "$tokenCount" ]; then
	confErrors="${confErrs}<configuration><database token=... /></configuration> is missing or not a number.\n"
fi

if [ ! -z "$confErrs" ]; then
	echo "Your <configuration> does not work:"
	echo -e "$confErrs"
	exit 7
fi

if [ ! -d "$outputDir" ]; then
	mkdir -p "$outputDir"
fi

webrootTarget=$outputDir/$webroot

if [ ! -d "$webrootTarget" ]; then
	echo "creating web root $webrootTarget"
	mkdir -p $webrootTarget
fi

outputFile=${webrootTarget}/${identifier}.html

if [ -e "$outputFile" ]; then
	echo "$outputFile exists, overwriting"
fi

outputFileDDL=${outputFile/.html/.ddl}
outputFilePHP=${outputFile/.html/.php}

echo "Transforming HTML"
saxonb-xslt -xsl:generator/transform_html_${language}.xsl -o:${outputFile} ${inputFile}
check_errors $?

echo "Transforming Database Schema"
saxonb-xslt -xsl:generator/transform_schema.xsl -o:${outputFileDDL} ${inputFile}
check_errors $?

echo "Transforming PHP"
saxonb-xslt -xsl:generator/transform_php_${language}.xsl -o:${outputFilePHP} ${inputFile}
check_errors $?

echo "Generating $tokenCount tokens"
outputFileTokens=${outputFile/.html/.dml}
output_tokens $tokenCount > $outputFileTokens

outputFileDB=$outputDir/$identifier.sqlite
echo "Initiating database $outputFileDB"

initFile=$identifier.init
echo ".bail on" >$initFile
echo ".read $outputFileDDL" >>$initFile
echo ".read $outputFileTokens" >>$initFile

errFn=errors_$(date +%d_%m_%Y-%H_%M_%S).log
sqlite3 -bail -init $initFile $outputFileDB '.exit' 1>/dev/null 2>$errFn
dbInitErr=$?
errLines=$(<$errFn wc -l)

if [ $dbInitErr -ne 0 ]; then
	echo "check $identifier.init"
	check_errors $dbInitErr
elif [ $errLines -gt 1 ]; then
	echo "check $identifier.init"
	echo "Errors in $errFn:"
	cat $errFn
	exit 7
else
	rm $initFile
	rm error*.log
fi


echo "copying .inc-files to $webrootTarget"
cp generator/hoyoque*.inc $webrootTarget

echo "copying stylesheet $stylesheet to $webrootTarget"
cp styles/$stylesheet $webrootTarget
