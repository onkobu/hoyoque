<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:exsl="http://exslt.org/common"
	xmlns:func="http://exslt.org/functions"
	extension-element-prefixes="exsl func">

	<xsl:output method="text" />

	<xsl:template match="/">
identifier=<xsl:value-of select="hoyoque/configuration/@identifier" />
language=<xsl:value-of select="hoyoque/configuration/@language" />
tokenCount=<xsl:value-of select="hoyoque/configuration/database/@tokens" />
relativeDir="<xsl:value-of select="hoyoque/configuration/database/@relativeDir" />"
webroot="<xsl:value-of select="hoyoque/configuration/@webroot" />"
stylesheet="<xsl:value-of select="hoyoque/configuration/html/@stylesheet" />"<xsl:text>
</xsl:text>
	</xsl:template>
</xsl:stylesheet>
