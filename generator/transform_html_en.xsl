<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:exsl="http://exslt.org/common"
	xmlns:func="http://exslt.org/functions"
	extension-element-prefixes="exsl func">

	<xsl:import href="i18n_en.xsl" />
	<xsl:import href="transform_html.xsl" />

	<xsl:template name="renderTokenInfo">
		<p class="info">
			Paste or type your personal token here. Without a token your answers cannot be processed.
		</p>
	</xsl:template>
</xsl:stylesheet>
