<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:exsl="http://exslt.org/common"
	xmlns:func="http://exslt.org/functions"
	extension-element-prefixes="exsl func">

	<xsl:template name="renderBodyFooter">
		<footer>
			<xsl:copy-of select="/hoyoque/foot/p" />
		</footer>
	</xsl:template>
	
</xsl:stylesheet>
