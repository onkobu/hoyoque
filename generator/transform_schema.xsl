<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:exsl="http://exslt.org/common"
	xmlns:func="http://exslt.org/functions"
	extension-element-prefixes="exsl func">

	<xsl:output method="text" />

	<xsl:template match="/">
		<xsl:variable name="tableName" select="hoyoque/configuration/@identifier" />
		<xsl:variable name="tokenCount" select="hoyoque/head/@tokens" />

DROP TABLE IF EXISTS <xsl:value-of select="$tableName" />;

CREATE TABLE <xsl:value-of select="$tableName" /> (
	id INTEGER	PRIMARY KEY AUTOINCREMENT,
	token CHAR(16),
<xsl:apply-templates select="hoyoque/section/question" />
);

DROP TABLE IF EXISTS hoyoque_token;

CREATE TABLE hoyoque_token (
	token char(16)
);

DROP TABLE IF EXISTS hoyoque_audit;

CREATE TABLE hoyoque_audit (
	aud_timestamp,
	aud_table,
	aud_action,
	aud_ref
);

DROP TABLE IF EXISTS hoyoque_config;

CREATE TABLE hoyoque_config (
	config_key VARCHAR(64) PRIMARY KEY,
	config_value VARCHAR(64) NOT NULL
);

INSERT INTO hoyoque_config (config_key, config_value) VALUES ('valid.from','<xsl:value-of select="hoyoque/configuration/@validFrom" />');
INSERT INTO hoyoque_config (config_key, config_value) VALUES ('valid.to','<xsl:value-of select="hoyoque/configuration/@validTo" />');

DROP TRIGGER IF EXISTS trigger_<xsl:value-of select="$tableName" />_ins;

CREATE TRIGGER trigger_<xsl:value-of select="$tableName" />_ins
	AFTER INSERT
	   ON <xsl:value-of select="$tableName" />
BEGIN
	INSERT INTO hoyoque_audit (
		   aud_timestamp,
		   aud_table,
		   aud_action,
		   aud_ref
		   ) VALUES (
		   	datetime('now'),
		   '<xsl:value-of select="$tableName" />',
			'INSERT',
			new.id
			);
END;

DROP TRIGGER IF EXISTS trigger_<xsl:value-of select="$tableName" />_upd;

CREATE TRIGGER trigger_<xsl:value-of select="$tableName" />_upd
	BEFORE UPDATE
	   ON <xsl:value-of select="$tableName" />
BEGIN
	SELECT RAISE(FAIL, "UPDATE disabled");
END;

DROP TRIGGER IF EXISTS trigger_<xsl:value-of select="$tableName" />_del;

CREATE TRIGGER trigger_<xsl:value-of select="$tableName" />_del
	BEFORE DELETE
	   ON <xsl:value-of select="$tableName" />
BEGIN
	SELECT RAISE(FAIL, "DELETE disabled");
END;

DROP VIEW IF EXISTS hoyoque_stat;

CREATE VIEW hoyoque_stat AS
	SELECT 'total' as "name", 'count' as "value", count(*) as "count" FROM <xsl:value-of select="$tableName" />
	<xsl:apply-templates select="hoyoque/section/question" mode="stat">
		<xsl:with-param name="tableName" select="$tableName" />
	</xsl:apply-templates>
;
	</xsl:template>

	<xsl:template match="question[@type='yesno']">
<xsl:value-of select="concat('	',@name)" /> CHAR(5)<xsl:if test="position() != last()" >,
</xsl:if>
	</xsl:template>

	<xsl:template match="question[@type='gender']">
<xsl:value-of select="concat('	', @name)" /> CHAR(6)<xsl:if test="position() != last()" >,
</xsl:if>
	</xsl:template>


	<xsl:template match="question[@type='oneofmany']">
<xsl:value-of select="concat('	', @name)" /> VARCHAR(64)<xsl:if test="position() != last()" >,
</xsl:if>
	</xsl:template>

	<xsl:template match="question[@type='manyofmany']">
<xsl:value-of select="concat('	', @name)" /> INTEGER<xsl:if test="position() != last()" >,
</xsl:if>
	</xsl:template>

	<xsl:template match="question[@type='text']">
<xsl:value-of select="concat('	', @name)" /> TEXT<xsl:if test="position() != last()" >,
</xsl:if>
	</xsl:template>


	<xsl:template match="question[starts-with(@type,'range')]">
<xsl:value-of select="concat('	', @name)" /> INTEGER<xsl:if test="position() != last()" >,
</xsl:if>
	</xsl:template>

	<!-- Questions of type text are more likely to differ. Their
		probability of equality does not make much sense. -->
	<xsl:template match="question[@type != 'text']" mode="stat">
		<xsl:param name="tableName" />
UNION
			SELECT '<xsl:value-of select="@name" />', <xsl:value-of select="@name" />, count(<xsl:value-of select="@name" />) FROM <xsl:value-of select="$tableName" /> GROUP BY <xsl:value-of select="@name" />
	</xsl:template>

	<xsl:template match="node() | text()" />

	<!-- drop text() from question[@type='text'] -->
	<xsl:template match="node() | text()" mode="stat" />
</xsl:stylesheet>
