<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:exsl="http://exslt.org/common"
	xmlns:func="http://exslt.org/functions"
	extension-element-prefixes="exsl func">

<xsl:import href="render_html_head.xsl" />
<xsl:import href="render_body_header.xsl" />
<xsl:import href="render_body_footer.xsl" />

	<xsl:output method="html" />

	<xsl:template match="/">
		<xsl:variable name="tableName" select="hoyoque/configuration/@identifier" />
		<xsl:variable name="confLocation" select="hoyoque/configuration/database/@relativeDir" />
<html>
		<xsl:call-template name="renderHtmlHead" />
	<body>
		<xsl:call-template name="renderBodyHeaderResult" />
		
	<section class="results">
	<ul>
		<li><xsl:value-of select="$messageTransferred" /></li>

<xsl:text disable-output-escaping="yes">&lt;</xsl:text>?php

include('hoyoque_functions.inc');

define('DB_PATH', '<xsl:value-of select="concat('sqlite:', $confLocation,$tableName,'.sqlite')" />');
$pdo=new PDO(DB_PATH);
$pdo-<xsl:text disable-output-escaping="yes">&gt;</xsl:text>setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
$pdo-<xsl:text disable-output-escaping="yes">&gt;</xsl:text>exec('PRAGMA foreign_keys = ON;');

define('STMT_TOKEN', 'FROM hoyoque_token WHERE token = :token');
define('STMT_SELECT_TOKEN', 'SELECT * '.STMT_TOKEN);
define('STMT_DELETE_TOKEN', 'DELETE '.STMT_TOKEN);
define('STMT_INSERT', '<xsl:value-of select="concat('INSERT INTO ', $tableName, '( token, ')" /><xsl:apply-templates select="hoyoque/section/question" mode="stmtCols">
		<xsl:with-param name="tableName" select="$tableName" />
		</xsl:apply-templates>) VALUES ( :token, <xsl:apply-templates select="hoyoque/section/question" mode="stmtVals" />)');
define('STMT_SELECT_CONFIG', 'SELECT config_value FROM hoyoque_config WHERE config_key=:key');
define('STMT_SELECT_STATS', 'SELECT * FROM hoyoque_stat');

/*
 * Reverse-lookup of questions with type manyofmany
 */
<xsl:apply-templates select="hoyoque/section/question[@type='manyofmany']" mode="decompressM2M" />

$now=new DateTime();

$validFrom=readConfigDate($pdo, 'valid.from');
$validTo=readConfigDate($pdo, 'valid.to');

$skipRest=FALSE;
$showStatistics=FALSE;

if ($now <xsl:text disable-output-escaping="yes">&lt;</xsl:text> $validFrom) {
	echo "<xsl:text disable-output-escaping="yes">&lt;/ul&gt;</xsl:text>The survey didn't start yet";
	$skipRest=TRUE;
}

if (!$skipRest <xsl:text disable-output-escaping="yes">&amp;&amp;</xsl:text> $now <xsl:text disable-output-escaping="yes">&gt;</xsl:text> $validTo) {
	echo "<xsl:text disable-output-escaping="yes">&lt;/ul&gt;&lt;/section&gt;</xsl:text>";
	echo '<section class="statistics">';
	showStatistics($pdo);
	echo '</section>';
	$skipRest=TRUE;
	$showStatistics=TRUE;
}
	
if (!$skipRest <xsl:text disable-output-escaping="yes">&amp;&amp;</xsl:text> ! isset($_POST['token'])) {
	echo '<li><xsl:value-of select="$messageTokenInvalid" /></li>';
	$skipRest=TRUE;
}

$hasToken=!$skipRest <xsl:text disable-output-escaping="yes">&amp;&amp;</xsl:text> hasToken($pdo);
	  
if (!$hasToken <xsl:text disable-output-escaping="yes">&amp;&amp;</xsl:text> !$showStatistics) {
    echo "<li>Das Token war ungültig</li>";
    $skipRest=TRUE;
}

if ($skipRest <xsl:text disable-output-escaping="yes">&amp;&amp;</xsl:text> !$showStatistics) {
  echo '<li><xsl:value-of select="$messageNothingSaved" /></li>';
} else {
	$pdo-<xsl:text disable-output-escaping="yes">&gt;</xsl:text>beginTransaction();
	<xsl:text disable-output-escaping="yes">echo "&lt;li&gt;</xsl:text><xsl:value-of select="$messageTokenValid" /><xsl:text disable-output-escaping="yes">&lt;/li&gt;";</xsl:text>
	$stmt = $pdo-<xsl:text disable-output-escaping="yes">&gt;</xsl:text>prepare(STMT_INSERT);
	$stmt-<xsl:text disable-output-escaping="yes">&gt;</xsl:text>bindValue(':token', $_POST['token']);
<xsl:apply-templates select="hoyoque/section/question" mode="stmtReplace" />
	$stmt-<xsl:text disable-output-escaping="yes">&gt;</xsl:text>execute();

	$stmt=$pdo-<xsl:text disable-output-escaping="yes">&gt;</xsl:text>prepare(STMT_DELETE_TOKEN);
	$stmt-<xsl:text disable-output-escaping="yes">&gt;</xsl:text>bindValue(':token', $_POST['token']);
	$stmt-<xsl:text disable-output-escaping="yes">&gt;</xsl:text>execute();
	$pdo-<xsl:text disable-output-escaping="yes">&gt;</xsl:text>commit();
  echo '<li><xsl:value-of select="$messageSaved" /></li>';
}
?<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
</ul></section>

<xsl:call-template name="renderBodyFooter" />

</body>
</html>
	</xsl:template>

	<xsl:template match="question" mode="stmtCols">
		<xsl:if test="position() != 1">, </xsl:if><xsl:value-of select="@name" />
	</xsl:template>

	<xsl:template match="question" mode="stmtVals">
		<xsl:if test="position() != 1">, </xsl:if>:<xsl:value-of select="@name" />
	</xsl:template>
	
	<xsl:template match="question" mode="stmtReplace">
$stmt-<xsl:text disable-output-escaping="yes">&gt;</xsl:text>bindValue(':<xsl:choose>
	<xsl:when test="@type='manyofmany'"><xsl:value-of select="@name" />', compress_manyofmany('<xsl:value-of select="@name" />', <xsl:value-of select="count(options/option)" />)</xsl:when>
			<xsl:otherwise><xsl:value-of select="@name" />', validate_<xsl:value-of select="@type" />($_POST['<xsl:value-of select="@name" />'])</xsl:otherwise>
	</xsl:choose>);</xsl:template>

	<xsl:template match="question" mode="decompressM2M">
define(VALUES_<xsl:value-of select="@name" />, array(
<xsl:for-each select="options/option">
	<xsl:if test="position() != 1">, </xsl:if>'<xsl:value-of select="text()" />'
	</xsl:for-each>
));
</xsl:template>

	<xsl:template match="node() | text()" />
</xsl:stylesheet>
