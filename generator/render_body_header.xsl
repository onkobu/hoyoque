<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:exsl="http://exslt.org/common"
	xmlns:func="http://exslt.org/functions"
	extension-element-prefixes="exsl func">

	<xsl:template name="renderBodyHeaderForm">
		<header>
			<xsl:apply-templates select="/hoyoque/head" mode="form" />
		</header>
	</xsl:template>
	
	<xsl:template match="head" mode="form">
		<h1><xsl:value-of select="title/text()" /></h1>
		<p>
			<xsl:value-of select="description/text()" />
		</p>
		<p>
			<xsl:value-of select="$labelStarts" /> <xsl:value-of select="/hoyoque/configuration/@validFrom" /> und
			<xsl:value-of select="$labelEnds" /> <xsl:value-of select="/hoyoque/configuration/@validTo" /> (UTC).
		</p>
	</xsl:template>

	<xsl:template name="renderBodyHeaderResult">
		<header>
			<xsl:apply-templates select="/hoyoque/head" mode="result" />
		</header>
	</xsl:template>
	
	<xsl:template match="head" mode="result">
		<h1><xsl:value-of select="title/text()" /></h1>
		<p>
			<xsl:value-of select="$messageThanks" />
		</p>
	</xsl:template>

</xsl:stylesheet>
