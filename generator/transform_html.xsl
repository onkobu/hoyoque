<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:exsl="http://exslt.org/common"
	xmlns:func="http://exslt.org/functions"
	extension-element-prefixes="exsl func">

	<xsl:import href="render_html_head.xsl" />
	<xsl:import href="render_body_header.xsl" />
	<xsl:import href="render_body_footer.xsl" />

	<xsl:output method="html" 
		omit-xml-declaration="yes" 
		indent="no" 
		doctype-system="about:legacy-compat"
		encoding="utf-8" 
		use-character-maps="html-entities" />

	<xsl:character-map name="html-entities">
		<!-- Saxon hat eine kaputte Transformation
			 und zaubert sich etwas mit US-ASCII
			 zurecht. Das bricht vollständig mit
			 UTF-8 und der März in der format-dateTime-Routine
			 wird Mojibake. Das biegen wir hiermit
			 wieder hin -->
		<xsl:output-character character="�" string="&amp;auml;" />
	</xsl:character-map>

	<xsl:template match="/">
		<html>
			<xsl:call-template name="renderHtmlHead" />

			<body>
				<xsl:call-template name="renderBodyHeaderForm" />

				<form method="post">
					<xsl:attribute name="action">
						<xsl:value-of select="concat(hoyoque/configuration/@identifier,'.php')" />
					</xsl:attribute>
					
					<label for="token">Token:</label>
					<input type="text" name="token" id="token"  maxlength="24"></input>
					<xsl:call-template name="renderTokenInfo" />

					<xsl:apply-templates select="/hoyoque/section" />

					<p></p>
					<input type="submit" ></input>
				</form>
				
				<xsl:call-template name="renderBodyFooter" />

			</body>
		</html>
	</xsl:template>

	<xsl:template match="section">
		<section><h1><span><xsl:value-of select="position()" /></span><xsl:value-of select="@title" /></h1>
			<xsl:apply-templates select="question" />
		</section>
	</xsl:template>

	<xsl:template match="question[@type='yesno']">
		<xsl:call-template name="renderLabel" />
		<xsl:variable name="noneId"><xsl:value-of select="concat(@name,'none')" />		</xsl:variable>
		<xsl:variable name="yesId"><xsl:value-of select="concat(@name,'yes')" />		</xsl:variable>
		<xsl:variable name="noId"><xsl:value-of select="concat(@name,'no')" /></xsl:variable>

		<input type="radio" value="none" checked="true">
			<xsl:attribute name="name"><xsl:value-of select="@name" /></xsl:attribute>
			<xsl:attribute name="id"><xsl:value-of select="$noneId" /></xsl:attribute>
		</input>
		<label>
			<xsl:attribute name="for"><xsl:value-of select="$noneId" /></xsl:attribute>
			<xsl:value-of select="$labelSkip" /></label>

		<input type="radio" value="yes">
			<xsl:attribute name="name"><xsl:value-of select="@name" /></xsl:attribute>
			<xsl:attribute name="id"><xsl:value-of select="$yesId" /></xsl:attribute>
		</input>
		<label>
			<xsl:attribute name="for"><xsl:value-of select="$yesId" /></xsl:attribute>
			<xsl:value-of select="$labelYes" /></label>

		<input type="radio" value="no">
			<xsl:attribute name="name"><xsl:value-of select="@name" /></xsl:attribute>
			<xsl:attribute name="id"><xsl:value-of select="$noId" /></xsl:attribute>
		</input>
		<label>
			<xsl:attribute name="for"><xsl:value-of select="$noId" /></xsl:attribute>
			<xsl:value-of select="$labelNo" /></label>

		<xsl:call-template name="renderInfo" />
	</xsl:template>

	<xsl:template match="question[@type='gender']">
		<xsl:call-template name="renderLabel" />
		<select>
			<xsl:attribute name="id"><xsl:value-of select="@name" /></xsl:attribute>
			<xsl:attribute name="name"><xsl:value-of select="@name" /></xsl:attribute>
			<option value="none" selected="true"><xsl:value-of select="$labelSkip" /></option>
			<option>Female</option>
			<option>Male</option>
		</select>
		<xsl:call-template name="renderInfo" />
	</xsl:template>
	
	<xsl:template match="question[@type='oneofmany']">
		<xsl:call-template name="renderLabel" />
		<select>
			<xsl:attribute name="id"><xsl:value-of select="@name" /></xsl:attribute>
			<xsl:attribute name="name"><xsl:value-of select="@name" /></xsl:attribute>
			<option value="none" selected="true"><xsl:value-of select="$labelSkip" /></option>
			<xsl:copy-of select="options/option" />
		</select>
		<xsl:call-template name="renderInfo" />
	</xsl:template>
	
	<xsl:template match="question[@type='manyofmany']">
		<xsl:call-template name="renderLabel" />
		<fieldset>
						<!-- do something with <legend>? -->
							<ul>
		<xsl:for-each select="options/option">
			<xsl:variable name="checkboxId" select="concat(../../@name, position())" />
			<li><input type="checkbox">
				<xsl:attribute name="id"><xsl:value-of select="$checkboxId" /></xsl:attribute>
				<xsl:attribute name="name"><xsl:value-of select="$checkboxId" /></xsl:attribute>
			</input>
			<label>
				<xsl:attribute name="for" select="$checkboxId" />
				<xsl:value-of select="text()" />
		</label></li>
		</xsl:for-each>
	</ul>
	</fieldset>
		<xsl:call-template name="renderInfo" />
	</xsl:template>


	<xsl:template match="question[@type='text']">
		<xsl:call-template name="renderLabel" />
		<textarea>
			<xsl:attribute name="id"><xsl:value-of select="@name" /></xsl:attribute>
			<xsl:attribute name="name"><xsl:value-of select="@name" /></xsl:attribute>
	
		</textarea>
		<xsl:call-template name="renderInfo" />
	</xsl:template>

	<xsl:template match="question[@type='range2to2']">
		<xsl:call-template name="renderRange">
			<xsl:with-param name="from" select="-2" />
			<xsl:with-param name="to" select="2" />
			<xsl:with-param name="name" select="@name" />
			<xsl:with-param name="label" select="label/text()" />
			<xsl:with-param name="info" select="info/text()" />
			<xsl:with-param name="class" select="'range5'" />
		</xsl:call-template>
	</xsl:template>

	<xsl:template match="question[@type='range0to10']">
		<xsl:call-template name="renderRange">
			<xsl:with-param name="from" select="0" />
			<xsl:with-param name="to" select="10" />
			<xsl:with-param name="name" select="@name" />
			<xsl:with-param name="label" select="label/text()" />
			<xsl:with-param name="info" select="info/text()" />
			<xsl:with-param name="class" select="'range11'" />
		</xsl:call-template>
	</xsl:template>

	<xsl:template match="question[@type='range1to5']">
		<xsl:call-template name="renderRange">
			<xsl:with-param name="from" select="1" />
			<xsl:with-param name="to" select="5" />
			<xsl:with-param name="name" select="@name" />
			<xsl:with-param name="label" select="label/text()" />
			<xsl:with-param name="info" select="info/text()" />
			<xsl:with-param name="class" select="'range5'" />
		</xsl:call-template>
	</xsl:template>

	<xsl:template match="question[@type='range1to6']">
		<xsl:call-template name="renderRange">
			<xsl:with-param name="from" select="1" />
			<xsl:with-param name="to" select="6" />
			<xsl:with-param name="name" select="@name" />
			<xsl:with-param name="label" select="label/text()" />
			<xsl:with-param name="info" select="info/text()" />
			<xsl:with-param name="class" select="'range6'" />
		</xsl:call-template>
	</xsl:template>

	<xsl:template match="question[@type='range6to1']">
		<xsl:call-template name="renderRange">
			<xsl:with-param name="from" select="1" />
			<xsl:with-param name="to" select="6" />
			<xsl:with-param name="name" select="@name" />
			<xsl:with-param name="label" select="label/text()" />
			<xsl:with-param name="info" select="info/text()" />
			<xsl:with-param name="class" select="'range6'" />
			<xsl:with-param name="swapLabels" select="'true'" />
		</xsl:call-template>
	</xsl:template>

	<xsl:template name="renderRange">
		<xsl:param name="from" />
		<xsl:param name="to" />
		<xsl:param name="name" />
		<xsl:param name="label" />
		<xsl:param name="info" />
		<xsl:param name="class" />
		<xsl:param name="swapLabels" />
		<p class="label">
			<xsl:value-of select="$label" />
		</p>
		<div>
			<xsl:attribute name="class" select="$class" />
			<div class="range_input">
				<xsl:choose>
					<xsl:when test="$swapLabels"><xsl:value-of select="$to" /></xsl:when>
					<xsl:otherwise><xsl:value-of select="$from" /></xsl:otherwise>
				</xsl:choose>
				<input type="radio">
					<xsl:attribute name="name"><xsl:value-of select="$name" /></xsl:attribute>
					<xsl:attribute name="id"><xsl:value-of select="concat($name,'1')" /></xsl:attribute>
					<xsl:attribute name="value"><xsl:value-of select="1" /></xsl:attribute>
				</input>
			</div>

		<xsl:for-each select="($from + 1) to ($to - 1)">
			<xsl:variable name="idx" select="position() + 1" />
			<xsl:variable name="lblId" select="concat($name,$idx)" />
			<div class="range_input">
			<input type="radio">
				<xsl:attribute name="name"><xsl:value-of select="$name" /></xsl:attribute>
				<xsl:attribute name="id"><xsl:value-of select="$lblId" /></xsl:attribute>
				<xsl:attribute name="value"><xsl:value-of select="$idx" /></xsl:attribute>
			</input>
			</div>
		</xsl:for-each>
		<div class="range_input">
				<input type="radio">
					<xsl:attribute name="name"><xsl:value-of select="$name" /></xsl:attribute>
					<xsl:attribute name="id"><xsl:value-of select="concat($name,$to)" /></xsl:attribute>
					<xsl:attribute name="value"><xsl:value-of select="$to" /></xsl:attribute>
				</input>
		<xsl:choose>
			<xsl:when test="$swapLabels"><xsl:value-of select="$from" /></xsl:when>
			<xsl:otherwise><xsl:value-of select="$to" /></xsl:otherwise>
	</xsl:choose></div>
		</div>

		<xsl:if test="$info">
			<p class="info"><xsl:value-of select="$info" /></p>
		</xsl:if>
	</xsl:template>

	<xsl:template name="renderLabel">
		<p class="label">
		<xsl:choose>
			<xsl:when test="label">
				<xsl:value-of select="label/text()" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="text()" />
			</xsl:otherwise>
		</xsl:choose>
		</p>
	</xsl:template>

	<xsl:template name="renderInfo">
		<xsl:if test="info">
			<p class="info">
				<xsl:value-of select="info/text()" />
		</p>
		</xsl:if>
	</xsl:template>

	<xsl:template match="node() | text()">
		<!-- drop all other content -->
	</xsl:template>
</xsl:stylesheet>
