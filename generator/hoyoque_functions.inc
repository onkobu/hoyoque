<?php

function hasToken($pdo) {
    $stmt=$pdo->prepare(STMT_SELECT_TOKEN);
    $stmt->bindValue(':token', $_POST['token']);
    $stmt->execute();
      
    $row=$stmt->fetch();

	return $row;	
}

function readConfig($pdo, $key) {
	$stmt=$pdo->prepare(STMT_SELECT_CONFIG);
	$stmt->bindValue(':key', $key);
	$stmt->execute();
	$row=$stmt->fetch();
	if ($row) {
		return $row['config_value'];
	}
	return null;
}

function readConfigDate($pdo, $key) {
	$dateStr=readConfig($pdo, $key);
	if ($dateStr) {
		return DateTime::createFromFormat(DateTime::ISO8601,$dateStr);
	}
	return null;
}

function showStatistics($pdo) {
	$stmt=$pdo->prepare(STMT_SELECT_STATS);
	$stmt->execute();
	echo '<table><thead><th>Context</th><th>Name</th><th>Count</th><th>decompressed</th></thead><tbody>';
	while($row=$stmt->fetch()) {
		echo '<tr><td>'.$row['name'].'</td><td>'.htmlspecialchars($row['value']).'</td><td>'.$row['count'].'</td><td>';
		$vArr=constant('VALUES_'.$row['name']);

		if ($vArr && is_array($vArr)) {
			echo decompress_manyofmany($vArr, $row['value']);
		} else {
			echo '-';
		}

		'</td></tr>';
	}
	echo '</tbody></table>';
}

function str_startswith($total, $start) {
    return $total[0] === $start[0]
        ? strncmp($total, $start, strlen($start)) === 0
        : false;
}

function validate_range1to5($value) {
	return validate_range(1, 5, $value);
}

function validate_range1to6($value) {
	return validate_range(1, 6, $value);
}

function validate_range6to1($value) {
	return validate_range(1, 6, $value);
}

function validate_range1to10($value) {
	return validate_range(1, 10, $value);
}

function validate_range0to10($value) {
	return validate_range(0, 10, $value);
}

function validate_range2to2($value) {
	return validate_range(-2, 2, $value);
}

function validate_range($lower, $upper, $value) {
	if (!is_numeric($value)) {
		return -1;
	}
	return $value >= $lower && $value <= $upper;
}

function validate_yesno($value) {
	return $value === 'yes' || $value === 'no' ? $value : 'none';
}

function validate_oneofmany($value) {
	return strlen($value) < 64 ? $value : 'none';
}

function validate_gender($value) {
	return $value === 'male' || $value === 'female' ? $value : 'none';
}

function validate_text($value) {
	return strlen($value) < 1024 ? $value : substr($value, 0, 1024);
}

function compress_manyofmany($name, $limit) {
	$result=0;
	for($idx=0; $idx < $limit; $idx++) {
		$vName=$name . ($idx+1);
		if ($_POST[$vName]) {
			$result |= (1 << $idx);
		}
	}
	return $result;
}

function decompress_manyofmany($vArray, $value) {
	$result="";
	$idx=0;
	$isFirst=1;
	while($value != 0) {
		if ($value & 1) {
			if ($isFirst===0) {
				$result = $result.', ';
			} else {
				$isFirst=0;
			}
			$result = $result . $vArray[$idx];
		}
		$idx++;
		$value = $value >> 1;
	}
	return $result;
}

?>
