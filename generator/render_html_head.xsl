<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:exsl="http://exslt.org/common"
	xmlns:func="http://exslt.org/functions"
	extension-element-prefixes="exsl func">

	<xsl:template name="renderHtmlHead">
			<head>
				<title>
					<xsl:value-of select="/hoyoque/head/title/text()" />
				</title>
				<meta charset="UTF-8" />
				<link rel="stylesheet">
					<xsl:attribute name="href">
						<xsl:value-of select="/hoyoque/configuration/html/@stylesheet" />
					</xsl:attribute>
				</link>
			</head>
	</xsl:template>
</xsl:stylesheet>
