<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:exsl="http://exslt.org/common"
	xmlns:func="http://exslt.org/functions"
	extension-element-prefixes="exsl func">

	<xsl:variable name="labelYes" select="'Ja'" />
	<xsl:variable name="labelNo" select="'Nein'" />
	<xsl:variable name="labelSkip" select="'Egal'" />
	<xsl:variable name="labelStarts" select="'Beginnt '" />
	<xsl:variable name="labelEnds" select="'endet '" />

	<xsl:variable name="messageThanks" select="'Danke für Ihre Teilnahme.'" />
	<xsl:variable name="messageTransferred" select="'Antworten übertragen'" />
	<xsl:variable name="messageTokenInvalid" select="'Sie müssen ein gültiges Token eingeben'" />
	<xsl:variable name="messageTokenValid" select="'gültiges Token'" />
	<xsl:variable name="messageNothingSaved" select="'Nichts gespeichert'" />
	<xsl:variable name="messageSaved" select="'Antworten gespeichert'" />

</xsl:stylesheet>
