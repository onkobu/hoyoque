<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:exsl="http://exslt.org/common"
	xmlns:func="http://exslt.org/functions"
	extension-element-prefixes="exsl func">

	<xsl:variable name="labelYes" select="'Yes'" />
	<xsl:variable name="labelNo" select="'No'" />
	<xsl:variable name="labelSkip" select="'Skip'" />
	<xsl:variable name="labelStarts" select="'Starts '" />
	<xsl:variable name="labelEnds" select="'and ends '" />

	<xsl:variable name="messageThanks" select="'Thank you for participation.'" />
	<xsl:variable name="messageTransferred" select="'Answers transmitted'" />
	<xsl:variable name="messageTokenInvalid" select="'You must enter a valid Token'" />
	<xsl:variable name="messageTokenValid" select="'valid Token'" />
	<xsl:variable name="messageNothingSaved" select="'Nothing saved'" />
	<xsl:variable name="messageSaved" select="'Answers saved'" />


</xsl:stylesheet>
